package com.example.precscan.ui.meshview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.view.ViewCompat
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.example.precscan.databinding.FragmentMeshViewBinding
import com.google.android.filament.Material
import dev.romainguy.kotlin.math.lookAt
import io.github.sceneview.SceneView
import io.github.sceneview.math.Direction
import io.github.sceneview.math.Position
import io.github.sceneview.math.Rotation
import io.github.sceneview.node.ModelNode
import io.github.sceneview.utils.setFullScreen
import kotlinx.coroutines.delay


class MeshViewFragment : Fragment() {
    private var _binding: FragmentMeshViewBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    lateinit var sceneView: SceneView
    lateinit var loadingView: View

    var isLoading = false
        set(value) {
            field = value
            loadingView.isGone = !value
        }

    val args: MeshViewFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMeshViewBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sceneView = binding.sceneView
        loadingView = binding.loadingView

        isLoading = true

        val modelNode = ModelNode(
            position = Position(z = -4.0f),
            rotation = Rotation(x = 35.0f)
        )
        sceneView.addChild(modelNode)

        sceneView.cameraNode.transform = lookAt(
            eye = modelNode.worldPosition.let {
                Position(x = it.x, y = it.y + 0.5f, z = it.z + 2.0f)
            },
            target = modelNode.worldPosition,
            up = Direction(y = 1.0f)
        )

        lifecycleScope.launchWhenCreated {
            val model = modelNode.loadModel(
                context = requireContext(),
                lifecycle = lifecycle,
                glbFileLocation = args.path,
                scaleToUnits = 1.0f,
                centerOrigin = Position(x = 0.0f, y = 0.0f, z = 0.0f),
            )

            model?.materialInstance?.setCullingMode(Material.CullingMode.NONE)

            isLoading = false
        }

        val fullscreenButton = binding.fullscreenButton
        val exitFullscreenButton = binding.exitFullscreenButton
        fullscreenButton.setOnClickListener {
//            binding.meshViewFragmentLayout.setFullScreen(
//                fullScreen = true,
//                hideSystemBars = true,
//                fitsSystemWindows = true
//            )
//            activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

            binding.meshViewFragmentLayout.fitsSystemWindows = true
            fullscreenButton.isVisible = false
            exitFullscreenButton.isVisible = true
        }

        exitFullscreenButton.setOnClickListener {
//            binding.meshViewFragmentLayout.setFullScreen(
//                fullScreen = false,
//                hideSystemBars = false,
//                fitsSystemWindows = false
//            )
//            activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

            binding.meshViewFragmentLayout.fitsSystemWindows = false
            fullscreenButton.isVisible = true
            exitFullscreenButton.isVisible = false
        }
    }
}