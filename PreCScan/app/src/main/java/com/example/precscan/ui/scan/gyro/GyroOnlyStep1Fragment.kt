package com.example.precscan.ui.scan.gyro

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.example.precscan.R
import com.example.precscan.databinding.FragmentScanGyroStepMeasureBinding
import com.google.android.material.bottomnavigation.BottomNavigationView


class GyroOnlyStep1Fragment : Fragment() {
    private var _binding: FragmentScanGyroStepMeasureBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val viewModel: GyroScanRecordViewModel by navGraphViewModels(R.id.navigation_scan_gyro_step_1)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentScanGyroStepMeasureBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onResume() {
        super.onResume()

        val bottomNavView = activity!!.findViewById<BottomNavigationView>(R.id.nav_view)
        bottomNavView.isVisible = false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val cancelBtn = binding.stepCancelButton
        cancelBtn.setOnClickListener {
            findNavController().popBackStack()
        }

        val nextBtn = binding.stepNextButton
        nextBtn.setOnClickListener {
            findNavController().navigate(R.id.action_scan_step_1_to_step_2_explanation)
        }
        nextBtn.isEnabled = false

        val horizontalCircEditText = binding.horizontalCircEditText
        val verticalCircEditText = binding.verticalCircEditText

        horizontalCircEditText.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                viewModel.setHorizontalCircumference(s.toString().toFloat())
            }
        })

        verticalCircEditText.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                viewModel.setVerticalCircumference(s.toString().toFloat())
            }
        })

        viewModel.record.observe(viewLifecycleOwner, Observer {
            if (!it.verticalCircumference.isNaN() && !it.horizontalCircumference.isNaN())
                nextBtn.isEnabled = true;
        })
    }

    override fun onPause() {
        super.onPause()
        val bottomNavView = activity!!.findViewById<BottomNavigationView>(R.id.nav_view)
        bottomNavView.isVisible = true
    }
}