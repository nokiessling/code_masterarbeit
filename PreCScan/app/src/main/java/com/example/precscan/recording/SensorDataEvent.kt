package com.example.precscan.recording

import kotlinx.serialization.Serializable

@Serializable
data class SensorDataEvent(
    val timestamp: Long,
    val x: Float,
    val y: Float,
    val z: Float)
