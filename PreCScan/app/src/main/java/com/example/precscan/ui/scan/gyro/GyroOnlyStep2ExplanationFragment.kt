package com.example.precscan.ui.scan.gyro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.example.precscan.R
import com.example.precscan.databinding.FragmentScanGyroStep2ExplanationBinding
import com.google.android.material.bottomnavigation.BottomNavigationView

class GyroOnlyStep2ExplanationFragment : Fragment() {
    private var _binding: FragmentScanGyroStep2ExplanationBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val viewModel: GyroScanRecordViewModel by navGraphViewModels(R.id.navigation_scan_gyro_step_1)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentScanGyroStep2ExplanationBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onResume() {
        super.onResume()

        val bottomNavView = activity!!.findViewById<BottomNavigationView>(R.id.nav_view)
        bottomNavView.isVisible = false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val prevBtn = binding.stepPreviousButton
        prevBtn.setOnClickListener {
            findNavController().popBackStack()
        }

        val nextBtn = binding.stepNextButton
        nextBtn.setOnClickListener {
            findNavController().navigate(R.id.action_scan_step_2_explanation_to_step_2_dir_1_left)
        }

        Toast.makeText(context, " values: ${viewModel.record.value!!.horizontalCircumference}, ${viewModel.record.value!!.verticalCircumference}", Toast.LENGTH_SHORT).show()
    }

    override fun onPause() {
        super.onPause()
        val bottomNavView = activity!!.findViewById<BottomNavigationView>(R.id.nav_view)
        bottomNavView.isVisible = true
    }
}