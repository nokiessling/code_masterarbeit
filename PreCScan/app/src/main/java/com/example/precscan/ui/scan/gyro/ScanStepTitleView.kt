package com.example.precscan.ui.scan.gyro

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.example.precscan.R

class ScanStepTitleView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {

    init {
        inflate(context, R.layout.scan_step_title, this)
        val numberView: TextView = findViewById(R.id.step_number)
        val titleView: TextView = findViewById(R.id.step_title)
        val dirView: TextView = findViewById(R.id.step_dir)

        // todo: this should do a default styling of the CardView, but enforcing it here prevents
        //  edits via xml

        attrs?.let {
            val attributes = context.obtainStyledAttributes(it, R.styleable.ScanStepTitleView)
            numberView.text = attributes.getString(R.styleable.ScanStepTitleView_step_number)
            titleView.text = attributes.getString(R.styleable.ScanStepTitleView_step_title)
            val dir = attributes.getString(R.styleable.ScanStepTitleView_step_direction)
            if (dir.isNullOrEmpty())
                dirView.isVisible = false
            else
                dirView.text = dir
            attributes.recycle()
        }
    }
}