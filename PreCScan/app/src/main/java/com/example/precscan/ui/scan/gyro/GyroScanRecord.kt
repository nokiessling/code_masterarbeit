package com.example.precscan.ui.scan.gyro

import com.example.precscan.recording.SensorDataEvent
import kotlinx.serialization.Serializable

@Serializable
data class GyroScanRecord(
    var horizontalCircumference: Float = Float.NaN,
    var verticalCircumference: Float = Float.NaN,
    var leftScanData: MutableList<SensorDataEvent> = mutableListOf(),
    var rightScanData: MutableList<SensorDataEvent> = mutableListOf(),
    var upScanData: MutableList<SensorDataEvent> = mutableListOf(),
    var downScanData: MutableList<SensorDataEvent> = mutableListOf(),
)
