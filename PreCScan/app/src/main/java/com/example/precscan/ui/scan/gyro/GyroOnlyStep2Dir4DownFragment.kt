package com.example.precscan.ui.scan.gyro

import android.content.Context
import android.hardware.SensorManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.example.precscan.R
import com.example.precscan.databinding.FragmentScanGyroStep2Dir4DownBinding
import com.example.precscan.recording.SensorRecorder
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class GyroOnlyStep2Dir4DownFragment : Fragment() {
    private var _binding: FragmentScanGyroStep2Dir4DownBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val viewModel: GyroScanRecordViewModel by navGraphViewModels(R.id.navigation_scan_gyro_step_1)
    private lateinit var sensorManager: SensorManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.sensorManager = activity?.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentScanGyroStep2Dir4DownBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onResume() {
        super.onResume()

        val bottomNavView = activity!!.findViewById<BottomNavigationView>(R.id.nav_view)
        bottomNavView.isVisible = false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sensorRecorder = SensorRecorder(this.sensorManager, SensorManager.SENSOR_DELAY_FASTEST)

        val startCard = binding.stepStartCard
        val stopCard = binding.stepStopCard

        startCard.isVisible = true
        stopCard.isVisible = false

        startCard.setOnClickListener {
            sensorRecorder.startRecording()
            startCard.isVisible = false
            stopCard.isVisible = true
        }

        stopCard.setOnClickListener {
            sensorRecorder.stopRecording()
            startCard.isVisible = true
            stopCard.isVisible = false
            viewModel.setDownScanData(sensorRecorder.getEvents())
        }

        viewModel.record.observe(viewLifecycleOwner, Observer {
            if (it.downScanData.isNotEmpty()) {
                findNavController().navigate(R.id.action_scan_step_2_dir_4_down_to_wait_for_server)
            }
        })
    }

    override fun onPause() {
        super.onPause()
        val bottomNavView = activity!!.findViewById<BottomNavigationView>(R.id.nav_view)
        bottomNavView.isVisible = true
    }
}