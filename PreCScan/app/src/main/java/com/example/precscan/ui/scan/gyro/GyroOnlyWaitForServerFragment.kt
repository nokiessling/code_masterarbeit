package com.example.precscan.ui.scan.gyro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.example.precscan.MobileNavigationDirections
import com.example.precscan.R
import com.example.precscan.databinding.FragmentScanGyroWaitForServerBinding
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import java.io.File
import java.net.HttpURLConnection
import java.net.URL
import java.nio.file.Path
import java.text.SimpleDateFormat
import java.util.*


class GyroOnlyWaitForServerFragment : Fragment() {
    private var _binding: FragmentScanGyroWaitForServerBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val viewModel: GyroScanRecordViewModel by navGraphViewModels(R.id.navigation_scan_gyro_step_1)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentScanGyroWaitForServerBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onResume() {
        super.onResume()

        val bottomNavView = activity!!.findViewById<BottomNavigationView>(R.id.nav_view)
        bottomNavView.isVisible = false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.viewModelScope.launch { getModelAndSwitchView() }
    }

    override fun onPause() {
        super.onPause()
        val bottomNavView = activity!!.findViewById<BottomNavigationView>(R.id.nav_view)
        bottomNavView.isVisible = true
    }

    private suspend fun getModelAndSwitchView() {
        val path = postGyroData()
        val action = MobileNavigationDirections.actionToMeshView(path.toString())
        findNavController().navigate(action)
    }

    @Suppress("BlockingMethodInNonBlockingContext")
    private suspend fun postGyroData(): Path = withContext(Dispatchers.IO) {
        val url = URL("http://192.168.123.134:5000/reconstruct")
        (url.openConnection() as HttpURLConnection).run {
            requestMethod = "POST"
            addRequestProperty("Content-Type", "application/json")
            doOutput = true

            val json = Json.encodeToString(GyroScanRecord.serializer(), viewModel.record.value!!)
            outputStream.use {
                it.write(json.toByteArray())
            }

            val path = SaveBinaryFile(inputStream.readBytes())

            return@withContext path
        }
    }

    private fun SaveBinaryFile(bytes: ByteArray): Path {
        val root: String = context!!.getExternalFilesDir(null).toString()
        val myDir = File("$root/scans")
        if (!myDir.exists()) {
            myDir.mkdirs()
        }
        val date = Calendar.getInstance().time
        val filename = "${SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(date)}.glb"
        val file = File(myDir, filename)
        file.writeBytes(bytes)

        return file.toPath()
    }
}