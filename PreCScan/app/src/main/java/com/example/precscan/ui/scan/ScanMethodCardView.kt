package com.example.precscan.ui.scan

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.view.marginStart
import com.example.precscan.R

class ScanMethodCardView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : CardView(context, attrs) {

    init {
        inflate(context, R.layout.scan_method_card_view, this)
        val titleView: TextView = findViewById(R.id.scan_method_title)
        val descriptionView: TextView = findViewById(R.id.scan_method_description)

        // todo: this should do a default styling of the CardView, but enforcing it here prevents
        //  edits via xml

        attrs?.let {
            val attributes = context.obtainStyledAttributes(it, R.styleable.ScanMethodCardView)
            titleView.text = attributes.getString(R.styleable.ScanMethodCardView_title)
            descriptionView.text = attributes.getString(R.styleable.ScanMethodCardView_description)
            attributes.recycle()
        }

        isClickable = true
    }
}