package com.example.precscan.ui.scan.gyro

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.precscan.recording.SensorDataEvent

class GyroScanRecordViewModel : ViewModel() {
    private val mutableRecord = MutableLiveData(GyroScanRecord())

    val record: LiveData<GyroScanRecord> get() = mutableRecord

    fun setHorizontalCircumference(circumference: Float) {
        mutableRecord.value = mutableRecord.value!!.copy(horizontalCircumference = circumference)
    }

    fun setVerticalCircumference(circumference: Float) {
        mutableRecord.value = mutableRecord.value!!.copy(verticalCircumference = circumference)
    }

    fun setLeftScanData(data: List<SensorDataEvent>) {
        mutableRecord.value = mutableRecord.value!!.copy(leftScanData = data.toMutableList())
    }

    fun setRightScanData(data: List<SensorDataEvent>) {
        mutableRecord.value = mutableRecord.value!!.copy(rightScanData = data.toMutableList())
    }

    fun setUpScanData(data: List<SensorDataEvent>) {
        mutableRecord.value = mutableRecord.value!!.copy(upScanData = data.toMutableList())
    }

    fun setDownScanData(data: List<SensorDataEvent>) {
        mutableRecord.value = mutableRecord.value!!.copy(downScanData = data.toMutableList())
    }
}