package com.example.precscan.ui.scan.gyro

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.example.precscan.R

class ScanStepStartStopCardView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : CardView(context, attrs) {

    init {
        inflate(context, R.layout.scan_step_start_stop_card, this)
        val imageView: ImageView = findViewById(R.id.step_start_stop_image)
        val titleView: TextView = findViewById(R.id.step_start_stop_title)
        val descriptionView: TextView = findViewById(R.id.step_start_stop_description)

        // todo: this should do a default styling of the CardView, but enforcing it here prevents
        //  edits via xml

        attrs?.let {
            val attributes = context.obtainStyledAttributes(it, R.styleable.ScanStepStartStopCardView)
            if (attributes.hasValue(R.styleable.ScanStepStartStopCardView_step_start_stop_image))
                imageView.setImageResource(attributes.getInt(R.styleable.ScanStepStartStopCardView_step_start_stop_image, -1))
            titleView.text = attributes.getString(R.styleable.ScanStepStartStopCardView_step_start_stop_title)
            descriptionView.text = attributes.getString(R.styleable.ScanStepStartStopCardView_step_start_stop_description)
            attributes.recycle()
        }

        isClickable = true
    }
}