package com.example.precscan.recording

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import java.io.File

class SensorRecorder(
    private var sensorManager: SensorManager,
    private var frequency: Int,
) : SensorEventListener {

    private var events = mutableListOf<SensorDataEvent>()

    fun startRecording() {
        events.clear()
        val sensor = this.sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)
        this.sensorManager.registerListener(this, sensor, this.frequency)
    }

    fun stopRecording() {
        this.sensorManager.unregisterListener(this)
    }

    fun getEvents() = events.toMutableList()

    override fun onSensorChanged(p0: SensorEvent?) {
        events.add(SensorDataEvent(
            p0!!.timestamp,
            p0.values[0],
            p0.values[1],
            p0.values[2]
        ))
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }
}