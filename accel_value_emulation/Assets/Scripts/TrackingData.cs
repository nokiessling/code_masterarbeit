using UnityEngine;

public readonly struct TrackingData
{
    public double Time { get; }
    public Vector3 AngleVelocity { get; }
    public Vector3 LinearAcceleration { get; }
    public Vector3 TruePosition { get; }

    public Vector3 Normal { get; }

    public TrackingData(double time, Vector3 angleVelocity, Vector3 linearAcceleration, Vector3 truePosition,
        Vector3 normal)
    {
        Time = time;
        AngleVelocity = angleVelocity;
        LinearAcceleration = linearAcceleration;
        TruePosition = truePosition;
        Normal = normal;
    }
}
