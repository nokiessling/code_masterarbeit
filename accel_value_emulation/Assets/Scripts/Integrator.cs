using UnityEngine;

public class Integrator
{
    public Vector3 Position => lastPosition;
    public Vector3 Velocity => lastVelocity;
    public Vector3 Acceleration => lastAcceleration;

    private Vector3 lastPosition = Vector3.zero;
    private Vector3 lastVelocity = Vector3.zero;
    private Vector3 lastAcceleration = Vector3.zero;

    public void Update(Vector3 acceleration, float deltaTime)
    {
        // var velocity = 0.5f * (lastAcceleration + acceleration) * deltaTime + lastVelocity;
        var velocity = acceleration * deltaTime + lastVelocity;
        var position = velocity * deltaTime + lastPosition;

        lastPosition = position;
        lastVelocity = velocity;
        lastAcceleration = acceleration;
    }

    public void UpdateVelocity(Vector3 velocity, float deltaTime)
    {
        var position = velocity * deltaTime + lastPosition;

        lastPosition = position;
        lastVelocity = velocity;
        lastAcceleration = Vector3.zero;
    }

    public void Reset()
    {
        lastPosition = Vector3.zero;
        lastVelocity = Vector3.zero;
        lastAcceleration = Vector3.zero;
    }

    public void ResetVelocityAndAcceleration()
    {
        lastVelocity = Vector3.zero;
        lastAcceleration = Vector3.zero;
    }
}
