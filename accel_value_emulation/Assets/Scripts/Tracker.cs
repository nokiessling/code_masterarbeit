using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class Tracker : MonoBehaviour
{
    public SteamVR_Action_Boolean recordingAction;
    private LinkedList<TrackingData> data = new();
    private SteamVR_Input_Sources inputSource;

    private Integrator integrator = new();
    private bool isReady;
    private bool isRecording;
    private Vector3 lastAccel = Vector3.zero;
    private Vector3 lastAngle = Vector3.zero;
    private Vector3 lastAngleVelocity = Vector3.zero;
    private Vector3 lastPos = Vector3.zero;
    private Vector3 lastVelocity = Vector3.zero;
    private Vector3 startPos = Vector3.zero;
    private double time;

    private void Reset()
    {
        lastAngle = Vector3.zero;
        lastAngleVelocity = Vector3.zero;
        lastPos = Vector3.zero;
        lastVelocity = Vector3.zero;
        lastAccel = Vector3.zero;
        time = 0.0;

        integrator.Reset();
        isReady = false;
        data.Clear();
    }

    private void Update()
    {
        if (!isRecording) return;

        if (!isReady)
        {
            lastAngle = transform.rotation.eulerAngles * Mathf.Deg2Rad;
            lastAngle = new Vector3(lastAngle.x, lastAngle.z, lastAngle.y);
            lastPos = transform.position;
            lastPos = new Vector3(lastPos.x, lastPos.z, lastPos.y);
            isReady = true;
            startPos = lastPos;
        }

        GenGyroData();
        GenLinAccelData();

        var normal = transform.up;
        normal = new Vector3(normal.x, normal.z, normal.y);
        data.AddLast(new TrackingData(time, lastAngleVelocity, lastAccel, lastPos, normal));

        time += Time.deltaTime;
    }

    private void FixedUpdate()
    {
        if (!isRecording) return;

        Debug.Log($"lin accel ({inputSource}): {lastAccel}m/s2, {lastVelocity}m/s");
        Debug.Log($"distance: {(integrator.Position).magnitude}m, velocity: {integrator.Velocity}, accel: {lastAccel}");
    }

    private void OnEnable()
    {
        inputSource = GetComponentInParent<SteamVR_Behaviour_Pose>().inputSource;
        recordingAction.AddOnStateDownListener(OnRecodingActionChange, inputSource);
    }

    private void OnDisable()
    {
        recordingAction.RemoveOnStateDownListener(OnRecodingActionChange, inputSource);
    }

    private void OnRecodingActionChange(SteamVR_Action_Boolean fromaction, SteamVR_Input_Sources fromsource)
    {
        if (!isRecording)
        {
            Reset();
        }
        else
        {
            Debug.Log($"True distance: {(lastPos - startPos).magnitude}");

            var secData = new LinkedList<TrackingData>(data);
            var temp = secData.First;
            secData.RemoveFirst();
            secData.AddLast(temp);
            data.Zip(secData, (d0, d1) =>
                    new Tuple<Vector3, Vector3>(d0.TruePosition, d1.TruePosition))
                .ToList()
                .ForEach(tuple =>
                    Debug.DrawLine(tuple.Item1, tuple.Item2, Color.red, 360f));

            var lines = new List<string>();
            lines.Add("time;linear_acceleration;angle_velocity;normal;true_position");
            data.ForEach(td =>
                lines.Add($"{td.Time};" +
                          $"{td.LinearAcceleration.x},{td.LinearAcceleration.y},{td.LinearAcceleration.z};" +
                          $"{td.AngleVelocity.x},{td.AngleVelocity.y},{td.AngleVelocity.z};" +
                          $"{td.Normal.x},{td.Normal.y},{td.Normal.z};" +
                          $"{td.TruePosition.x},{td.TruePosition.y},{td.TruePosition.z}"));

            File.WriteAllLines(
                $"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}\\{Time.time}",
                lines);
        }

        isRecording = !isRecording;
    }

    private void GenGyroData()
    {
        var angle = transform.rotation.eulerAngles * Mathf.Deg2Rad;
        angle = new Vector3(angle.x, angle.z, angle.y);

        lastAngleVelocity = (lastAngle - angle) / Time.deltaTime;
        lastAngle = angle;
    }

    private void GenLinAccelData()
    {
        var pos = transform.position;
        pos = new Vector3(pos.x, pos.z, pos.y);

        var diffPos = pos - lastPos;

        var velocity = diffPos / Time.deltaTime;
        var accel = (velocity - lastVelocity) / Time.deltaTime;

        lastPos = pos;
        lastVelocity = velocity;
        lastAccel = accel;
    }
}