package com.example.recordsensordata.recording

data class SensorDataEvent(
    val sensorType: SensorType,
    val timestamp: Long,
    val x: Float,
    val y: Float,
    val z: Float)
