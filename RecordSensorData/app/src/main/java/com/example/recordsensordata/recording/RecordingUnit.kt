package com.example.recordsensordata.recording

import android.hardware.SensorEventListener

interface RecordingUnit : SensorEventListener{
    fun start() {}
    fun stop() {}
}