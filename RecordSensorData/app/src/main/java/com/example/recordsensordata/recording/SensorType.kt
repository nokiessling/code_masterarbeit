package com.example.recordsensordata.recording

import android.hardware.Sensor

enum class SensorType(val id: Int) {
    ACCELEROMETER(0),
    GYROSCOPE(1),
    MAGNETOMETER(2),
    LINEAR_ACCELERATION(3);

    fun toAndroidConstant(): Int = when (this) {
        ACCELEROMETER -> Sensor.TYPE_ACCELEROMETER
        GYROSCOPE -> Sensor.TYPE_GYROSCOPE
        MAGNETOMETER -> Sensor.TYPE_MAGNETIC_FIELD
        LINEAR_ACCELERATION -> Sensor.TYPE_LINEAR_ACCELERATION
    }

    companion object {
        fun valueOfAndroidConstant(constant: Int): SensorType = when (constant) {
            Sensor.TYPE_ACCELEROMETER -> SensorType.ACCELEROMETER
            Sensor.TYPE_GYROSCOPE -> SensorType.GYROSCOPE
            Sensor.TYPE_MAGNETIC_FIELD -> SensorType.MAGNETOMETER
            Sensor.TYPE_LINEAR_ACCELERATION -> SensorType.LINEAR_ACCELERATION
            else -> throw IllegalArgumentException("$constant is currently not supported!")
        }
    }
}