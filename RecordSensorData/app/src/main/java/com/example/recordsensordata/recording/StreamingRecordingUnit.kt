package com.example.recordsensordata.recording

import android.hardware.Sensor
import android.hardware.SensorEvent
import java.io.OutputStream
import java.net.Socket
import java.nio.ByteBuffer
import java.util.concurrent.ConcurrentLinkedQueue

class StreamingRecordingUnit(private val hostname: String, private val port: Int) : RecordingUnit {

    private val dataStreamer = DataStreamer()

    override fun start() {
        Thread(this.dataStreamer).start()
    }

    override fun stop() {
        this.dataStreamer.stop()
    }

    override fun onSensorChanged(p0: SensorEvent?) {
        this.dataStreamer.add(
            SensorDataEvent(
                SensorType.valueOfAndroidConstant(p0!!.sensor!!.type),
                p0.timestamp,
                p0.values[0],
                p0.values[1],
                p0.values[2]
            )
        )
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }

    inner class DataStreamer : Runnable {
        @Volatile
        private var stopExecution = false
        private var queue = ConcurrentLinkedQueue<SensorDataEvent>()

        override fun run() {
            stopExecution = false
            val socket = Socket(hostname, port)
            socket.use {
                val outputStream = socket.getOutputStream()
                while (!stopExecution) {
                    sendEvents(outputStream)
                }
            }

            queue.clear()
        }

        private fun sendEvents(outputStream: OutputStream) {
            while (queue.isNotEmpty()) {
                val event = queue.poll()!!
                outputStream.write(
                    ByteBuffer.allocate(4 + 8 + 3 * 4)
                        .putInt(event.sensorType.id)
                        .putLong(event.timestamp)
                        .putFloat(event.x)
                        .putFloat(event.y)
                        .putFloat(event.z)
                        .array()
                )
            }
        }

        fun add(event: SensorDataEvent) {
            if (stopExecution) return
            this.queue.add(event)
        }

        fun stop() {
            this.stopExecution = true
        }
    }
}