package com.example.recordsensordata.recording

import android.hardware.Sensor
import android.hardware.SensorEvent
import java.io.File

class CsvRecordingUnit(private val file: File) : RecordingUnit {
    private var events = mutableListOf<SensorDataEvent>()

    override fun start() {
        events.clear()
    }

    override fun stop() {
        val buffer = StringBuffer()

        events.forEach{
            buffer.append("${it.sensorType.id}:${it.timestamp},${it.x},${it.y},${it.z}\n")
        }
        file.writeText(buffer.toString())
    }

    override fun onSensorChanged(p0: SensorEvent?) {
        events.add(SensorDataEvent(
            SensorType.valueOfAndroidConstant(p0!!.sensor!!.type),
            p0.timestamp,
            p0.values[0],
            p0.values[1],
            p0.values[2]
        ))
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }
}