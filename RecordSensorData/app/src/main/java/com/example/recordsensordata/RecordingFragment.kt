package com.example.recordsensordata

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.preference.PreferenceManager
import com.example.recordsensordata.databinding.RecordingFragmentBinding
import com.example.recordsensordata.recording.SensorType
import com.example.recordsensordata.recording.SensorRecorder
import java.io.File

class RecordingFragment : Fragment() {

    private var _binding: RecordingFragmentBinding? = null
    private lateinit var sensorManager: SensorManager
    private lateinit var sensorRecorder: SensorRecorder

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sensorManager = activity?.getSystemService(Context.SENSOR_SERVICE) as SensorManager

        val deviceSensors: List<Sensor> = sensorManager.getSensorList(Sensor.TYPE_ALL)
        deviceSensors.forEach {
            println(it)
        }

        println("Accelerometer: ${sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)}")
        println("Gyroskop: ${sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)}")
        println("Magnetometer: ${sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)}")
        println("Linear Acceleration: ${sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION)}")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = RecordingFragmentBinding.inflate(inflater, container, false)
        return binding.root

    }

    private fun splitHostnameAndPort(hostnameAndPort: String?): Pair<String?, Int> {
        var hostname = ""
        var port = 1234

        if (hostnameAndPort != null) {
            if (hostnameAndPort.contains(":")) {
                port = hostnameAndPort.substringAfterLast(":").toInt()
                hostname = hostnameAndPort.substringBeforeLast(":")
            } else {
                hostname = hostnameAndPort
            }
        }

        return Pair(hostname, port)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val startBtn = binding.buttonStartRecording
        val stopBtn = binding.buttonStopRecording
        startBtn.setOnClickListener {
            val sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(activity!!.baseContext/* Activity context */)

            val remoteRecording = sharedPreferences.getBoolean("recording_type_switch", false)
            val hostnameAndPort = sharedPreferences.getString("hostname_port_text", "")
            val (hostname, port) = splitHostnameAndPort(hostnameAndPort)
            val filename = sharedPreferences.getString("filename", "")
            val frequency = sharedPreferences.getString("recording_frequency_list", "")
            val customFrequency =
                sharedPreferences.getString("custom_frequency_text", "0")!!.toInt()

            val recordAccelerometer = sharedPreferences.getBoolean("accelerometer_check_box", false)
            val recordGyroscope = sharedPreferences.getBoolean("gyroscope_check_box", false)
            val recordMagnetometer = sharedPreferences.getBoolean("magnetometer_check_box", false)
            val recordLinearAcceleration =
                sharedPreferences.getBoolean("linear_acceleration_check_box", false)

            val sensors = mutableListOf<SensorType>()

            if (recordAccelerometer) {
                sensors.add(SensorType.ACCELEROMETER)
            }
            if (recordGyroscope) {
                sensors.add(SensorType.GYROSCOPE)
            }
            if (recordMagnetometer) {
                sensors.add(SensorType.MAGNETOMETER)
            }
            if (recordLinearAcceleration) {
                sensors.add(SensorType.LINEAR_ACCELERATION)
            }

            println(
                "remoteRecording=$remoteRecording, " +
                        "hostnameAndPort=$hostnameAndPort, " +
                        "frequency=$frequency, " +
                        "customFrequency=$customFrequency, " +
                        "recordAccelerometer=$recordAccelerometer, " +
                        "recordGyroscope=$recordGyroscope, " +
                        "recordMagnetometer=$recordMagnetometer, " +
                        "recordLinearAcceleration=$recordLinearAcceleration"
            )

            if (sensors.isEmpty()) {
                return@setOnClickListener
            }

            this.sensorRecorder = SensorRecorder(
                this.sensorManager,
                when (frequency) {
                    "fastest" -> SensorManager.SENSOR_DELAY_FASTEST
                    "normal" -> SensorManager.SENSOR_DELAY_NORMAL
                    else -> customFrequency
                },
                sensors,
                if (remoteRecording && hostname != "") hostname else null,
                port,
                if (!remoteRecording && filename != "") File(
                    context!!.getExternalFilesDir(null),
                    filename + ".csv"
                ) else null
            )
            this.sensorRecorder.startRecording()

            startBtn.isVisible = false
            stopBtn.isVisible = true
        }

        stopBtn.setOnClickListener {
            stopBtn.isVisible = false
            startBtn.isVisible = true
            this.sensorRecorder.stopRecording()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}