package com.example.recordsensordata

import android.os.Bundle
import android.text.InputType
import android.view.Menu
import androidx.preference.EditTextPreference
import androidx.preference.ListPreference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val settingsItem = menu.findItem(R.id.SettingsFragment)
        if (settingsItem != null) {
            settingsItem.isVisible = false
        }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)

        val recordingTypeSwitch = findPreference<SwitchPreference>("recording_type_switch")
        val hostnamePortText = findPreference<EditTextPreference>("hostname_port_text")
        val filenameText = findPreference<EditTextPreference>("filename")
        filenameText?.isVisible = !recordingTypeSwitch!!.isChecked
        hostnamePortText?.isVisible = recordingTypeSwitch.isChecked
        recordingTypeSwitch.setOnPreferenceChangeListener { preference, newValue ->
            filenameText?.isVisible = !(newValue as Boolean)
            hostnamePortText?.isVisible = newValue as Boolean
            true
        }

        val recordingFrequencyList = findPreference<ListPreference>("recording_frequency_list")
        val customFrequencyText = findPreference<EditTextPreference>("custom_frequency_text")
        customFrequencyText?.isVisible = customFrequencyText?.text == "custom"
        recordingFrequencyList?.setOnPreferenceChangeListener { preference, newValue ->
            customFrequencyText?.isVisible = newValue == "custom"
            true
        }

        customFrequencyText?.setOnBindEditTextListener { editText ->
            editText.inputType = InputType.TYPE_CLASS_NUMBER
        }
    }
}