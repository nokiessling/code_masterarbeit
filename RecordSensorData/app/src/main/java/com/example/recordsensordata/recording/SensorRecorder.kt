package com.example.recordsensordata.recording

import android.hardware.Sensor
import android.hardware.SensorManager
import java.io.File

class SensorRecorder(
    private var sensorManager: SensorManager,
    private var frequency: Int,
    sensorTypes: List<SensorType>,
    hostname: String? = null,
    port: Int = 1234,
    filename: File? = null
) {
    private var sensorTypes : List<SensorType>
    private var recordingUnit: RecordingUnit

    init {
        this.sensorTypes = sensorTypes.toList()
        if (hostname != null) {
            this.recordingUnit = StreamingRecordingUnit(hostname, port)
        } else if (filename != null) {
            this.recordingUnit = CsvRecordingUnit(filename)
        } else {
            throw IllegalArgumentException("Either hostname or filename must be supplied!")
        }
    }

    fun startRecording() {
        this.recordingUnit.start()
        for (sensorType in sensorTypes) {
            val sensor = this.sensorManager.getDefaultSensor(sensorType.toAndroidConstant())
            this.sensorManager.registerListener(this.recordingUnit, sensor, this.frequency)
        }
    }

    fun stopRecording() {
        this.sensorManager.unregisterListener(this.recordingUnit)
        this.recordingUnit.stop()
    }
}