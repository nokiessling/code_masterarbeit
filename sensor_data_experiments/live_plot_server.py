#!/usr/bin/env python3
import collections
import logging
import queue
import socket
import struct
import threading
from collections.abc import Iterable
from enum import IntEnum
from typing import NamedTuple, List, Dict, Tuple, Optional, Set, Protocol, Sequence

import matplotlib.axes
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation

logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)


# region sensor data server
class SensorType(IntEnum):
    ACCELEROMETER = 0
    GYROSCOPE = 1
    MAGNETOMETER = 2
    LINEAR_ACCELERATION = 3
    ORIENTATION = 100
    VELOCITY = 101
    DISTANCE = 102
    GYRO_ORIENTATION_RAD = 103
    GYRO_ORIENTATION_DEG = 104
    GRAVITY = 105

    @staticmethod
    def from_int(num: int) -> "SensorType":
        for sensor in SensorType:
            if sensor == num:
                return sensor

        raise ValueError(f"{num} is not a valid SensorType value!")

    def value_range(self) -> Optional[Tuple[float, float]]:
        ranges = {
            SensorType.GYRO_ORIENTATION_RAD: (0., 2. * np.pi),
            SensorType.GYRO_ORIENTATION_DEG: (0., 360.),
        }

        return ranges.get(self, None)


class SensorEvent(NamedTuple):
    sensor_id: SensorType
    time: int
    x: float
    y: float
    z: float


def start_server(
    sensor_data_queue: queue.Queue, port: int = 1234, recv_buffer_len: int = 4096
):
    event_struct = struct.Struct("!iqfff")  # sensor_id, time, x, y, z

    logging.info(f"starting plot_server at port {port}...")
    with socket.create_server(("", port)) as server:
        logging.info(f"starting listening now")
        server.listen(0)  # we only accept one connection
        while True:
            logging.info(f"waiting for a connection now...")
            client_socket, client_address = server.accept()
            logging.info(f"connection established with {client_address}")

            tailing_data = b""
            while True:
                buffer = tailing_data + client_socket.recv(recv_buffer_len)

                if not buffer:
                    logging.info(
                        f"received no data. closing dangling connection with {client_address}"
                    )
                    break

                num_msgs = len(buffer) // event_struct.size
                logger.debug(f"received {num_msgs} msgs len(buffer) = {len(buffer)}")
                for i in range(num_msgs):
                    args = list(event_struct.unpack_from(buffer, event_struct.size * i))
                    args[0] = SensorType.from_int(args[0])
                    event = SensorEvent(*args)
                    logging.debug(f"received the following data: {event}")
                    sensor_data_queue.put(event)

                tailing_data = buffer[num_msgs * event_struct.size :]


# endregion

# region additional sensor data generation and handling
class SensorDataBuffer:
    def __init__(self, id: int, max_len: int = 2000):
        self._id = id
        self._time = collections.deque(maxlen=max_len)
        self._x = collections.deque(maxlen=max_len)
        self._y = collections.deque(maxlen=max_len)
        self._z = collections.deque(maxlen=max_len)
        self._lock = threading.Lock()

    def append(self, event: SensorEvent):
        logging.debug(f"acquiring lock for append... {self._lock.locked()}")
        with self._lock:
            self._time.append(event.time)
            self._x.append(event.x)
            self._y.append(event.y)
            self._z.append(event.z)
            logging.debug("finished append")

    def latest_n_elements(
        self, num: int
    ) -> Tuple[List[int], List[float], List[float], List[float]]:
        logging.debug(f"acquiring lock for latest_n_data... {self._lock.locked()}")
        with self._lock:
            logging.debug("finished latest_n_data")
            return (
                list(self._time)[-num:],
                list(self._x)[-num:],
                list(self._y)[-num:],
                list(self._z)[-num:],
            )

    @property
    def data(self) -> Tuple[List[int], List[float], List[float], List[float]]:
        logging.debug(f"acquiring lock for combined_Data... {self._lock.locked()}")
        with self._lock:
            logging.debug("finished combined_data")
            return list(self._time), list(self._x), list(self._y), list(self._z)

    @property
    def data_head(self) -> Optional[Tuple[int, float, float, float]]:
        logging.debug(f"acquiring lock for data_head... {self._lock.locked()}")
        with self._lock:
            logging.debug("finished data_head")
            if not self._time:
                return None
            return self._time[-1], self._x[-1], self._y[-1], self._z[-1]

    def __len__(self) -> int:
        return len(self._time)


class SensorDataBufferManager:
    def __init__(self):
        self._buffers: Dict[SensorType, SensorDataBuffer] = {
            sensor: SensorDataBuffer(id=sensor) for sensor in SensorType
        }

    def append(self, event: SensorEvent):
        if event.sensor_id not in self._buffers:
            raise ValueError(f"received not supported sensor_id {event.sensor_id}")

        self._buffers[event.sensor_id].append(event)

    def get(self, sensor: SensorType) -> SensorDataBuffer:
        buffer = self._buffers.get(sensor, None)
        if buffer is None:
            raise ValueError(f"received not supported sensor_id {sensor}")
        return buffer


def generate_ori_from_gyro(
    last_sensor_data: SensorEvent, buffer_manager: SensorDataBufferManager
):
    if last_sensor_data.sensor_id != SensorType.GYROSCOPE.value:
        return

    gyro_buffer = buffer_manager.get(SensorType.GYROSCOPE)
    gyro_ori_buffer = buffer_manager.get(SensorType.GYRO_ORIENTATION_RAD)

    if len(gyro_buffer) < 2:
        return

    last_ori_event = (0.0, 0.0, 0.0)
    if len(gyro_ori_buffer) > 0:
        last_ori_event = gyro_ori_buffer.data_head[1:]

    gyro_value_pair = buffer_manager.get(SensorType.GYROSCOPE).latest_n_elements(2)

    current_gyro = (
        gyro_value_pair[0][1],
        gyro_value_pair[1][1],
        gyro_value_pair[2][1],
        gyro_value_pair[3][1],
    )
    previous_gyro = (
        gyro_value_pair[0][0],
        gyro_value_pair[1][0],
        gyro_value_pair[2][0],
        gyro_value_pair[3][0],
    )

    time_diff = (current_gyro[0] - previous_gyro[0]) * 1e-9

    new_ori_event = (
        (time_diff * 0.5 * (previous_gyro[1] + current_gyro[1]) + last_ori_event[0])
        % (2 * np.pi),
        (time_diff * 0.5 * (previous_gyro[2] + current_gyro[2]) + last_ori_event[1])
        % (2 * np.pi),
        (time_diff * 0.5 * (previous_gyro[3] + current_gyro[3]) + last_ori_event[2])
        % (2 * np.pi),
    )

    buffer_manager.append(
        SensorEvent(
            sensor_id=SensorType.GYRO_ORIENTATION_RAD,
            time=current_gyro[0],
            x=new_ori_event[0],
            y=new_ori_event[1],
            z=new_ori_event[2],
        )
    )

    buffer_manager.append(
            SensorEvent(
                    sensor_id=SensorType.GYRO_ORIENTATION_DEG,
                    time=current_gyro[0],
                    x=np.rad2deg(new_ori_event[0]),
                    y=np.rad2deg(new_ori_event[1]),
                    z=np.rad2deg(new_ori_event[2]),
            )
    )


def create_gravity_sensor_data(
    last_sensor_data: SensorEvent, buffer_manager: SensorDataBufferManager
):
    # TODO: only works if the gyro ori calculation started with the phone flat on a surface
    if last_sensor_data.sensor_id != SensorType.GYROSCOPE.value:
        return

    gyro_ori_buffer = buffer_manager.get(SensorType.GYRO_ORIENTATION_RAD)

    if len(gyro_ori_buffer) == 0:
        return

    current_ori = gyro_ori_buffer.data_head
    norm_ori = np.asarray(current_ori[1:]) / 2 * np.pi
    gravity = norm_ori
    # gravity = norm_ori * -9.81

    buffer_manager.append(
        SensorEvent(
            sensor_id=SensorType.GRAVITY,
            time=current_ori[0],
            x=gravity[0],
            y=gravity[1],
            z=gravity[2],
        )
    )


class DataGeneratorFunc(Protocol):
    def __call__(
        self, last_sensor_data: SensorEvent, buffer_manager: SensorDataBufferManager
    ):
        pass


def start_buffer_manager(
    sensor_data_queue: queue.Queue,
    buffer_manager: SensorDataBufferManager,
    generator_funcs: Optional[Sequence[DataGeneratorFunc]],
):

    if not generator_funcs:
        generator_funcs = list()

    logging.info("buffer manager waiting...")
    while True:
        sensor_data: SensorEvent = sensor_data_queue.get()
        logging.debug("adding new event data to buffers")
        buffer_manager.append(sensor_data)

        for func in generator_funcs:
            func(last_sensor_data=sensor_data, buffer_manager=buffer_manager)


# endregion

# region continues plotting
def continues_plot(
    buffer_manager: SensorDataBufferManager, interval: int, sensors: Sequence[SensorType]
):
    fig, ax = plt.subplots(3, len(sensors))

    sensor_to_ax: Dict[SensorType, List[matplotlib.axes.Axes]] = dict()
    for sens_type in sensors:
        sensor_to_ax[sens_type] = [None, None, None]
    for i, row_ax in enumerate(ax):
        for col_ax, sens_type in zip(
            row_ax if isinstance(row_ax, Iterable) else [row_ax], list(sensors)
        ):
            sensor_to_ax[sens_type][i] = col_ax
            col_ax.ticklabel_format(useOffset=False, style="plain")
            col_ax.set_xlim([0, 360])

    def _plot(frame: int, *args, **kwargs):
        for sens_type in sensors:
            buffer = buffer_manager.get(sens_type)

            time, x, y, z = buffer.data

            sensor_to_ax[sens_type][0].cla()
            sensor_to_ax[sens_type][1].cla()
            sensor_to_ax[sens_type][2].cla()
            value_range = sens_type.value_range()
            if value_range:
                sensor_to_ax[sens_type][0].set_ylim(*value_range)
                sensor_to_ax[sens_type][1].set_ylim(*value_range)
                sensor_to_ax[sens_type][2].set_ylim(*value_range)
            sensor_to_ax[sens_type][0].plot(time, x)
            sensor_to_ax[sens_type][1].plot(time, y)
            sensor_to_ax[sens_type][2].plot(time, z)
            sensor_to_ax[sens_type][0].set_title(f"{sens_type.name} x")
            sensor_to_ax[sens_type][1].set_title(f"{sens_type.name} y")
            sensor_to_ax[sens_type][2].set_title(f"{sens_type.name} z")

    animation = FuncAnimation(fig, _plot, interval=int(interval))
    plt.show()


# endregion


def main():
    sensor_data_queue = queue.Queue()

    server_thread = threading.Thread(
        target=start_server, kwargs=dict(sensor_data_queue=sensor_data_queue, port=1234)
    )
    server_thread.start()

    buffer_manager = SensorDataBufferManager()
    buffer_manager_thread = threading.Thread(
        target=start_buffer_manager,
        kwargs=dict(
            sensor_data_queue=sensor_data_queue,
            buffer_manager=buffer_manager,
            generator_funcs=[
                # if a new func is added the sensors argument of continues_plot
                # also needs to be updated
                generate_ori_from_gyro,
                #create_gravity_sensor_data,
            ],
        ),
    )
    buffer_manager_thread.start()

    continues_plot(
        buffer_manager=buffer_manager,
        interval=100,
        # sensors also has to contain the computed sensors
        sensors=[
            SensorType.GYROSCOPE,
            SensorType.GYRO_ORIENTATION_DEG,
            # SensorType.GYRO_ORIENTATION_RAD,
            #SensorType.GRAVITY,
        ],
    )

    server_thread.join()
    buffer_manager_thread.join()


if __name__ == "__main__":
    main()
