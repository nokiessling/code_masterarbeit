import functools
from typing import Dict, Tuple, List, Callable

import matplotlib.pyplot as plt
import pandas as pd

import sensor_data_experiments.data as data
import sensor_data_experiments.calculation as calc


def print_distance_table(
    data_load_func: Callable[[str], data.ExperimentDF],
    no_linear_acceleration: bool = False,
    no_accelerometer: bool = False,
):
    def add_result(results_dict: Dict, df: pd.DataFrame, key: Tuple[str]):
        pos_df, velo_df = calc.calc_distance(df)
        results_dict[key] = dict(
            time=pos_df.iloc[-1].timestamp - pos_df.iloc[0].timestamp,
            x_distance=pos_df.iloc[-1].x,
            y_distance=pos_df.iloc[-1].y,
            z_distance=pos_df.iloc[-1].z,
            x_velocity=velo_df.iloc[-1].x,
            y_velocity=velo_df.iloc[-1].y,
            z_velocity=velo_df.iloc[-1].z,
        )

    phones = ["px6", "op7p"]
    movement_method = ["lift_off", "slide", "car", "saw_railing"]
    distance_variants = ["25cm", "50cm"]
    speed_variants = ["slow", "fast"]

    results = dict()
    for phone in phones:
        for speed in speed_variants:
            for distance in distance_variants:
                for movement in movement_method:
                    df = data_load_func(f"{phone}_{movement}_{distance}_right_{speed}")

                    partial_key = (phone, speed, distance, movement)
                    if not no_linear_acceleration:
                        add_result(
                            results_dict=results,
                            df=df.loc[
                                df.type == data.SensorType.LINEAR_ACCELERATION.value
                            ],
                            key=tuple(
                                list(partial_key)
                                + [data.SensorType.LINEAR_ACCELERATION.name]
                            ),
                        )
                    if not no_accelerometer:
                        add_result(
                            results_dict=results,
                            df=df.loc[df.type == data.SensorType.ACCELEROMETER.value],
                            key=tuple(
                                list(partial_key) + [data.SensorType.ACCELEROMETER.name]
                            ),
                        )

    print(
        ";".join(
            [
                "phone",
                "speed",
                "distance",
                "movement",
                "sensor",
                "time",
                "x_distance",
                "y_distance",
                "z_distance",
                "x_velocity",
                "y_velocity",
                "z_velocity",
            ]
        ),
    )

    for variant_tuple, value_dict in results.items():
        print(";".join(variant_tuple), end="")
        print(
            f";{value_dict['time']:0.2f} s"
            f";{value_dict['x_distance']:0.2f} m"
            f";{value_dict['y_distance']:0.2f} m"
            f";{value_dict['z_distance']:0.2f} m"
            f";{value_dict['x_velocity']:0.2f} m/s"
            f";{value_dict['y_velocity']:0.2f} m/s"
            f";{value_dict['z_velocity']:0.2f} m/s",
        )


if __name__ == "__main__":

    # df = data.load_experiment_csv(data.exp_csv_path(f"{phone}_{movement}_{distance}_right_{speed}"))

    print("plain")
    print("-" * 80)
    print_distance_table(
        data_load_func=lambda p: data.load_experiment_csv(data.exp_csv_path(p))
    )
    print()

    print("sliding window with a size of 10")
    print("-" * 80)
    print_distance_table(
        data_load_func=lambda p: calc.apply_sliding_window(
            df=data.load_experiment_csv(data.exp_csv_path(p)), window_size=10
        )
    )
    print()

    print("sliding window with a size of 30")
    print("-" * 80)
    print_distance_table(
        data_load_func=lambda p: calc.apply_sliding_window(
            df=data.load_experiment_csv(data.exp_csv_path(p)), window_size=10
        )
    )
    print()

    print("offset and gain calibration applied for accelerometer")
    print("-" * 80)
    print_distance_table(
        data_load_func=lambda e: calc.apply_accelerometer_offset_and_gain_calibration(
            value_df=data.load_experiment_csv(data.exp_csv_path(e)),
            calib_data=calc.CalibrationData.build_for_experiment(e),
        )[0],
        no_linear_acceleration=True,
    )
    print()
