#!/usr/bin/env python3
import tempfile
from pathlib import Path
from typing import Sequence, Tuple

import numpy as np
import open3d as o3d
import pyvista as pv
from matplotlib import pyplot as plt
import CGAL.CGAL_Kernel as K
import CGAL.CGAL_Point_set_3 as Ps3
import CGAL.CGAL_Polyhedron_3 as Poly3
import CGAL.CGAL_Advancing_front_surface_reconstruction as Afsr

import sensor_data_experiments.data as data


def test_open3d(
    points: np.ndarray,
    normals: np.ndarray,
    alphas_to_test: Sequence[float],
    radii_to_test: Sequence[Tuple[float, float, float, float]],
    depths_to_test: Sequence[int],
):
    pcd: o3d.geometry.PointCloud = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(points)
    pcd.normals = o3d.utility.Vector3dVector(normals)
    o3d.visualization.draw_geometries(
        [pcd], window_name="Open3D raw points", mesh_show_back_face=True
    )

    # alpha shape [Edelsbrunner1983]
    tetra_mesh, pt_map = o3d.geometry.TetraMesh().create_from_point_cloud(pcd)
    for alpha in alphas_to_test:
        name = f"Open3Ds Alpha Shape implementation with alpha={alpha}"
        print(name)
        alpha_mesh = o3d.geometry.TriangleMesh().create_from_point_cloud_alpha_shape(
            pcd, alpha, tetra_mesh, pt_map
        )
        alpha_mesh.compute_vertex_normals()
        o3d.visualization.draw_geometries(
            [alpha_mesh], window_name=name, mesh_show_back_face=True
        )

    # ball pivoting algorithm (BPA) [Bernardini1999]
    for radii in radii_to_test:
        name = f"Open3Ds Ball Pivoting Algorithm (BPA) implementation with radiis={','.join([str(r) for r in radii])}"
        print(name)
        rec_mesh = o3d.geometry.TriangleMesh().create_from_point_cloud_ball_pivoting(
            pcd, o3d.utility.DoubleVector(radii)
        )
        o3d.visualization.draw_geometries(
            [pcd, rec_mesh], window_name=name, mesh_show_back_face=True
        )

    # Poisson surface reconstruction method [Kazhdan2006]
    for depth in depths_to_test:
        name = (
            f"Open3Ds Poisson Surface Reconstruction implementation with depth={depth}"
        )
        print(name)

        (
            poisson_mesh,
            densities,
        ) = o3d.geometry.TriangleMesh().create_from_point_cloud_poisson(
            pcd=pcd, depth=9
        )
        o3d.visualization.draw_geometries(
            [pcd, poisson_mesh],
            window_name=name,
            mesh_show_back_face=True,
        )

        densities = np.asarray(densities)
        density_colors = plt.get_cmap("plasma")(
            (densities - densities.min()) / (densities.max() - densities.min())
        )
        density_colors = density_colors[:, :3]
        density_mesh = o3d.geometry.TriangleMesh()
        density_mesh.vertices = poisson_mesh.vertices
        density_mesh.triangles = poisson_mesh.triangles
        density_mesh.triangle_normals = poisson_mesh.triangle_normals
        density_mesh.vertex_colors = o3d.utility.Vector3dVector(density_colors)
        o3d.visualization.draw_geometries(
            [density_mesh],
            window_name=name + " and density based vertex color",
            mesh_show_back_face=True,
        )


def test_pyvista(
    points: np.ndarray,
    alphas_to_test: Sequence[float],
    num_neighbours_to_test: Sequence[int],
):
    cloud = pv.PolyData(points)
    cloud.plot()

    for alpha in alphas_to_test:
        name = f"pyvistas 3D Delaunay triangulation implementation with alpha={alpha}"
        print(name)

        volume = cloud.delaunay_3d(alpha=alpha)
        shell = volume.extract_geometry()
        shell.plot(text=name)

    for num_neighbours in num_neighbours_to_test:
        name = f"pyvistas 3D reconstruct surface method with num_neighbours={num_neighbours} and sample_spacing=0.002"
        print(name)
        surf = cloud.reconstruct_surface(nbr_sz=num_neighbours, sample_spacing=0.002)

        pl = pv.Plotter()
        pl.add_mesh(surf, color=True, show_edges=True)
        pl.add_title(name)

        pl.show()


def test_cgal(
    points: np.ndarray,
    normals: np.ndarray,
    radius_ratio_bound_to_test: Sequence[float],
    beta_to_test: Sequence[float],
):
    def _reconstruct(radius_ratio_bound: float = 5.0, beta: float = 0.52):
        name = f"CGALs advancing front surface construction algorithm with {radius_ratio_bound=} and {beta=}"
        print(name)

        with tempfile.TemporaryDirectory() as out_dir_name:
            model_file = Path(out_dir_name) / "tmp.off"

            p: Poly3.Polyhedron_3 = Poly3.Polyhedron_3()
            Afsr.advancing_front_surface_reconstruction(
                point_set, p, radius_ratio_bound, beta
            )
            p.write_to_file(model_file.as_posix())

            mesh = o3d.io.read_triangle_mesh(model_file.as_posix())
            o3d.visualization.draw_geometries(
                [mesh], mesh_show_back_face=True, window_name=""
            )

    point_set = Ps3.Point_set_3()
    point_set.add_normal_map()

    for i in range(len(points)):
        point_set.insert(
            K.Point_3(points[i][0], points[i][1], points[i][2]),
            K.Vector_3(normals[i][0], normals[i][1], normals[i][2]),
        )

    for radius_ratio_bound in radius_ratio_bound_to_test:
        _reconstruct(radius_ratio_bound=radius_ratio_bound)

    for beta in beta_to_test:
        _reconstruct(beta=beta)


def main():
    scan_df = data.load_unity_recording_csv(
        data.exp_csv_path("unity_half_sphere_scan_1")
    )
    points = scan_df.loc[
        scan_df.type == data.SensorType.DEBUG_TRUE_POSITION.value, ["x", "y", "z"]
    ].to_numpy()
    normals = scan_df.loc[
        scan_df.type == data.SensorType.DEBUG_NORMAL.value, ["x", "y", "z"]
    ].to_numpy()

    test_pyvista(
        points=points,
        alphas_to_test=np.logspace(np.log10(0.5), np.log10(0.01), num=4),
        num_neighbours_to_test=[20, 80, 320, 1280],
    )

    test_cgal(
        points=points,
        normals=normals,
        radius_ratio_bound_to_test=[2.5, 5.0, 7.5],
        beta_to_test=[0.26, 0.52, 0.78],
    )

    test_open3d(
        points=points,
        normals=normals,
        alphas_to_test=np.logspace(np.log10(0.5), np.log10(0.01), num=4),
        radii_to_test=[
            (0.01, 0.02, 0.04, 0.08),
            (0.02, 0.04, 0.08, 0.16),
            (0.04, 0.08, 0.16, 0.32),
            (0.08, 0.16, 0.32, 0.64),
        ],
        depths_to_test=[2, 5, 7, 9],
    )


if __name__ == "__main__":
    main()
