from dataclasses import dataclass
from typing import Tuple, Dict

import pandas
import pandas as pd
import matplotlib.pyplot as plt

from sensor_data_experiments import data


@dataclass
class CalibrationData:
    positive_x: data.ExperimentDF
    negative_x: data.ExperimentDF
    positive_y: data.ExperimentDF
    negative_y: data.ExperimentDF
    positive_z: data.ExperimentDF
    negative_z: data.ExperimentDF

    @classmethod
    def build(cls, phone_name: str) -> "CalibrationData":
        mapping = {
            "right_side_up": "positive_x",
            "left_side_up": "negative_x",
            "top_up": "positive_y",
            "bottom_up": "negative_y",
            "face_up": "positive_z",
            "face_down_clipped": "negative_z",
        }

        out_kwargs = dict()
        for file_id, field_name in mapping.items():
            df = data.load_experiment_csv(
                data.exp_csv_path(f"{phone_name}_calib_{file_id}")
            )

            out_kwargs[field_name] = df

        return cls(**out_kwargs)

    @classmethod
    def build_load_for_experiment(cls, experiment_name: str) -> "CalibrationData":
        phone_name = experiment_name.split("_")[0]
        return cls.build(phone_name=phone_name)

    @property
    def x_pair(self) -> Tuple[data.ExperimentDF, data.ExperimentDF]:
        return self.positive_x, self.negative_x

    @property
    def y_pair(self) -> Tuple[data.ExperimentDF, data.ExperimentDF]:
        return self.positive_y, self.negative_y

    @property
    def z_pair(self) -> Tuple[data.ExperimentDF, data.ExperimentDF]:
        return self.positive_z, self.negative_z


def axis_means(df: pandas.DataFrame) -> Tuple[float]:
    df = df.mean()
    return df.x, df.y, df.z


def apply_accelerometer_offset_and_gain_calibration(
    value_df: data.ExperimentDF, calib_data: CalibrationData
) -> Tuple[data.ExperimentDF, Dict, Dict]:
    # based on https://www.digikey.it/it/articles/using-an-accelerometer-for-inclination-sensing

    calibrated_df = value_df.copy(True)

    offsets = dict()
    gains = dict()

    for axis_name in ["x", "y", "z"]:
        pos_df, neg_df = getattr(calib_data, f"{axis_name}_pair")
        pos_df = pos_df.loc[pos_df.type == data.SensorType.ACCELEROMETER.value]
        neg_df = neg_df.loc[neg_df.type == data.SensorType.ACCELEROMETER.value]

        pos_axis_mean = pos_df[axis_name].mean()
        neg_axis_mean = neg_df[axis_name].mean()

        offsets[axis_name] = 0.5 * (pos_axis_mean + neg_axis_mean)
        gains[axis_name] = 0.5 * (pos_axis_mean - neg_axis_mean) / 9.81

        accel_rows = calibrated_df.type == data.SensorType.ACCELEROMETER.value
        calibrated_df.loc[[accel_rows], axis_name] -= offsets[axis_name]
        calibrated_df[[accel_rows], axis_name] /= gains[axis_name]

    return calibrated_df, offsets, gains


def main():

    sensor = data.SensorType.ACCELEROMETER
    calib_data = CalibrationData.build("op7p", sensor)
    value_df = data.load_experiment_csv(data.exp_csv_path("op7p_slide_25cm_right_fast"))
    # value_df = data.load_experiment_csv(data.exp_csv_path("op7p_saw_railing_25cm_right_fast"))
    # value_df = data.load_experiment_csv(data.exp_csv_path("op7p_lift_off_25cm_right_fast"))
    # value_df = data.load_experiment_csv(data.exp_csv_path("op7p_car_25cm_right_fast"))

    # value_df = value_df.loc[value_df.type == sensor.value]
    #
    # value_df.plot(x="timestamp", y=["x", "y", "z"], title="before")
    # value_df, offsets, gains = unnamed_calibration(value_df, calib_data)
    # value_df.plot(x="timestamp", y=["x", "y", "z"], title="after")
    #
    # plt.show()

    value_df = data.load_experiment_csv(data.exp_csv_path("px6_calib_face_up"))
    value_df = value_df.loc[value_df.type == sensor.value]
    value_df.plot(x="timestamp", y=["x", "y", "z"], title="face_up")

    value_df = data.load_experiment_csv(
        data.exp_csv_path("px6_calib_face_down_clipped")
    )
    value_df = value_df.loc[value_df.type == sensor.value]
    value_df.plot(x="timestamp", y=["x", "y", "z"], title="1")

    value_df = data.load_experiment_csv(
        data.exp_csv_path("px6_calib_face_down_2_clipped")
    )
    value_df = value_df.loc[value_df.type == sensor.value]
    value_df.plot(x="timestamp", y=["x", "y", "z"], title="2")

    value_df = data.load_experiment_csv(
        data.exp_csv_path("px6_calib_face_down_3_clipped")
    )
    value_df = value_df.loc[value_df.type == sensor.value]
    value_df.plot(x="timestamp", y=["x", "y", "z"], title="3")

    value_df = data.load_experiment_csv(
        data.exp_csv_path("px6_calib_face_down_4_clipped")
    )
    value_df = value_df.loc[value_df.type == sensor.value]
    value_df.plot(x="timestamp", y=["x", "y", "z"], title="4")

    value_df = data.load_experiment_csv(data.exp_csv_path("px6_calib_face_down_bridge"))
    value_df = value_df.loc[value_df.type == sensor.value]
    value_df.plot(x="timestamp", y=["x", "y", "z"], title="bridge")

    value_df = data.load_experiment_csv(
        data.exp_csv_path("px6_calib_face_down_bridge_2")
    )
    value_df = value_df.loc[value_df.type == sensor.value]
    value_df.plot(x="timestamp", y=["x", "y", "z"], title="bridge_2")

    plt.show()

    # direction = ["face_up", "face_down_clipped", "top_up", "bottom_up", "left_side_up", "right_side_up"]
    #
    # for cur_dir in direction:
    #     df = data.load_experiment_csv(data.exp_csv_path(f"op7p_calib_{cur_dir}"))
    #     df = df.loc[df.type == data.SensorType.ACCELEROMETER.value]
    #     means = axis_means(df)
    #     print(f"{cur_dir}: {means[0]:.3f}, {means[1]:.3f}, {means[2]:.3f}, sum: {sum(means)}")


if __name__ == "__main__":
    main()
