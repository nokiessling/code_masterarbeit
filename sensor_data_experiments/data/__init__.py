from enum import Enum
from pathlib import Path

import pandas as pd
import pandera as pa


class SensorType(Enum):
    ACCELEROMETER = 0
    GYROSCOPE = 1
    MAGNETOMETER = 2
    LINEAR_ACCELERATION = 3
    ORIENTATION = 100
    VELOCITY = 101
    DISTANCE = 102
    GYRO_ORIENTATION = 103
    GRAVITY = 104
    DEBUG_NORMAL = 200
    DEBUG_TRUE_POSITION = 201


class ExperimentSchema(pa.SchemaModel):
    type: pa.typing.Series[int] = pa.Field()
    timestamp: pa.typing.Series[float] = pa.Field()
    x: pa.typing.Series[float] = pa.Field()
    y: pa.typing.Series[float] = pa.Field()
    z: pa.typing.Series[float] = pa.Field()


ExperimentDF = pa.typing.DataFrame[ExperimentSchema]


def exp_csv_path(name: str) -> Path:
    """
    Tries to find an experiment csv file with name `name` from the sensor_data_experiments/data
    dir and returns the corresponding path. If it fails to find such a file it will raise an
    ValueError.
    """

    if not name.endswith(".csv"):
        name += ".csv"

    root = Path(__file__).parent
    csv_file = root / name

    if not csv_file.exists():
        raise ValueError(
            f"{name} points to no .csv file in sensor_data_experiments/data"
        )

    return csv_file


@pa.check_types
def load_experiment_csv(file: Path, start_timestamp_at_0: bool = True) -> ExperimentDF:
    """
    Loads csv experiment `file` and returns it as a DataFrame from pandas. This function handles
    the 4 and 5 column based formats and will raise an ValueError if the file contains more or
    less columns.
    """

    df = pd.read_csv(file, sep=",", header=None)

    num_cols = len(df.columns)
    possible_cols = ["type", "timestamp", "x", "y", "z"]

    if num_cols == 5:
        mapper = {i: v for i, v in enumerate(possible_cols)}
    elif num_cols == 4:
        mapper = {i: v for i, v in enumerate(possible_cols[1:])}
    else:
        raise ValueError(f"{file} is not a experiment csv")

    df = df.rename(mapper=mapper, axis=1)

    df.timestamp = df.timestamp * 1e-9

    if start_timestamp_at_0:
        df.timestamp -= df.timestamp.iat[0]

    return df


@pa.check_types
def load_unity_recording_csv(file: Path, start_pos_at_0: bool = True) -> ExperimentDF:
    """
    Loads a debug recording csv `file` and returns it as a DataFrame from pandas. The returned
    the DataFrame has the same format as the one returned by `load_experiment_csv()`. If the
    csv file is not in the correct format a ValueError gets raised.
    """

    def build_sub_dataframe(
        source_df: pd.DataFrame, field_name: str, type_value: int
    ) -> pd.DataFrame:
        df = source_df[["time", field_name]].copy()

        # expanding of single triple column to x, y and z columns
        df[["x", "y", "z"]] = df[field_name].str.split(",", expand=True)
        del df[field_name]
        df["type"] = type_value
        df = df.astype({c: "float64" for c in df.columns})

        df = df.rename(columns={"time": "timestamp"})

        return df

    df = pd.read_csv(file, sep=";", header=0)

    columns = df.columns.tolist()

    needed_columns = {
        "time",
        "linear_acceleration",
        "angle_velocity",
        "normal",
        "true_position",
    }
    if not needed_columns.issubset(columns):
        raise ValueError(f"{file} is not in the debug recording csv format")

    df = pd.concat(
        [
            build_sub_dataframe(
                df, "linear_acceleration", SensorType.LINEAR_ACCELERATION.value
            ),
            build_sub_dataframe(df, "angle_velocity", SensorType.GYROSCOPE.value),
            build_sub_dataframe(
                df, "true_position", SensorType.DEBUG_TRUE_POSITION.value
            ),
            build_sub_dataframe(df, "normal", SensorType.DEBUG_NORMAL.value),
        ]
    )

    df = df.sort_values(by=["timestamp", "type"])
    # clean index rebuild based on new sorting
    df = df.reset_index(drop=True)

    if start_pos_at_0:
        pos_df = df.loc[df.type == SensorType.DEBUG_TRUE_POSITION.value]
        pos_df = pos_df.copy()
        pos_df.x -= pos_df.x.iat[0]
        pos_df.y -= pos_df.y.iat[0]
        pos_df.z -= pos_df.z.iat[0]
        df.update(pos_df)

    # column reordering
    df = df[["type", "timestamp", "x", "y", "z"]]

    # type fixing
    df = df.astype(
        {
            "type": "int64",
            "timestamp": "float64",
            "x": "float64",
            "y": "float64",
            "z": "float64",
        }
    )

    return df
