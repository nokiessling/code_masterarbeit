#!/usr/bin/env bash

set -e

python_package_dir=$(python -c "from distutils import sysconfig; print(sysconfig.get_python_lib())")
build_dir="/tmp"
repo_name="cgal-swig-bindings"
repo_dir="${build_dir}/${repo_name}"

echo "This script will install the CGAL python bindings into the python packaging directory:"
echo
echo ${python_package_dir}\"
echo
read -p "Do you want to continue? [y/n] " -n 1 -r
echo
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo "Aborting installation now"
    exit 0
fi

if [ -d ${repo_dir} ]; then
    rm -rf ${repo_dir}
fi

if [ -d "${python_package_dir}/CGAL" ]; then
    echo "CGAL is already installed. Exiting now"
    exit 1
fi

# building
echo "Building CGAL python binding now"
cd ${build_dir}
git clone https://github.com/CGAL/cgal-swig-bindings.git
cd ${repo_name}
cmake -DBUILD_PYTHON=ON -DBUILD_JAVA=OFF .
# make -j $(nproc)
make -j 4
echo

# installation
echo "Installing CGAL python binding now"
cp -r build-python/CGAL ${python_package_dir}
cp lib/*.so ${python_package_dir}/CGAL/
echo

# cleanup
echo "Cleaning up after CGAL python binding building now"
rm -rf ${repo_dir}
echo

echo "Successfully installed CGAL python binding"
exit 0
