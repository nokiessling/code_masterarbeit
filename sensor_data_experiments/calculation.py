from dataclasses import dataclass
from typing import Tuple, Dict, List

import pandas as pd
import pandera as pa

import sensor_data_experiments.data as data


class ResultSchema(pa.SchemaModel):
    timestamp: pa.typing.Series[float] = pa.Field()
    x: pa.typing.Series[float] = pa.Field()
    y: pa.typing.Series[float] = pa.Field()
    z: pa.typing.Series[float] = pa.Field()


ResultDF = pa.typing.DataFrame[ResultSchema]
VelocityDF = ResultDF
PositionDF = ResultDF


@pa.check_types
def calc_distance(df: data.ExperimentDF) -> Tuple[PositionDF, VelocityDF]:
    df_iterator = df.itertuples()
    first_row = next(df_iterator)

    velocities = [[0.0, 0.0, 0.0]]
    positions = [[0.0, 0.0, 0.0]]
    timestamps = [first_row.timestamp]

    for row in df_iterator:
        delta_time = row.timestamp - timestamps[-1]

        velo = [
            row.x * delta_time + velocities[-1][0],
            row.y * delta_time + velocities[-1][1],
            row.z * delta_time + velocities[-1][2],
        ]
        pos = [
            velo[0] * delta_time + positions[-1][0],
            velo[1] * delta_time + positions[-1][1],
            velo[2] * delta_time + positions[-1][2],
        ]

        timestamps.append(row.timestamp)
        velocities.append(velo)
        positions.append(pos)

    postion_df = pd.DataFrame(positions, columns=["x", "y", "z"])
    postion_df["timestamp"] = pd.Series(timestamps)
    postion_df = postion_df[["timestamp", "x", "y", "z"]]

    velocity_df = pd.DataFrame(velocities, columns=["x", "y", "z"])
    velocity_df["timestamp"] = pd.Series(timestamps)
    velocity_df = velocity_df[["timestamp", "x", "y", "z"]]

    return postion_df, velocity_df


@pa.check_types
def apply_sliding_window(df: data.ExperimentDF, window_size: int) -> data.ExperimentDF:
    # copy old df and remove the rows with not enough elements for the sliding window
    new_df = df.iloc[window_size - 1 :, :]
    new_df = new_df.copy()

    unique_types = new_df.type.unique()
    for type_num in unique_types:
        type_df = df.loc[df.type == type_num]

        new_df.x.update(type_df.x.rolling(window=window_size).mean().dropna())
        new_df.y.update(type_df.y.rolling(window=window_size).mean().dropna())
        new_df.z.update(type_df.z.rolling(window=window_size).mean().dropna())

    return new_df


@dataclass
class CalibrationData:
    positive_x: data.ExperimentDF
    negative_x: data.ExperimentDF
    positive_y: data.ExperimentDF
    negative_y: data.ExperimentDF
    positive_z: data.ExperimentDF
    negative_z: data.ExperimentDF

    @classmethod
    def build(cls, phone_name: str) -> "CalibrationData":
        mapping = {
            "right_side_up": "positive_x",
            "left_side_up": "negative_x",
            "top_up": "positive_y",
            "bottom_up": "negative_y",
            "face_up": "positive_z",
            "face_down_clipped": "negative_z",
        }

        out_kwargs = dict()
        for file_id, field_name in mapping.items():
            df = data.load_experiment_csv(
                data.exp_csv_path(f"{phone_name}_calib_{file_id}")
            )

            out_kwargs[field_name] = df

        return cls(**out_kwargs)

    @classmethod
    def build_for_experiment(cls, experiment_name: str) -> "CalibrationData":
        phone_name = experiment_name.split("_")[0]
        return cls.build(phone_name=phone_name)

    @property
    def x_pair(self) -> Tuple[data.ExperimentDF, data.ExperimentDF]:
        return self.positive_x, self.negative_x

    @property
    def y_pair(self) -> Tuple[data.ExperimentDF, data.ExperimentDF]:
        return self.positive_y, self.negative_y

    @property
    def z_pair(self) -> Tuple[data.ExperimentDF, data.ExperimentDF]:
        return self.positive_z, self.negative_z


def apply_accelerometer_offset_and_gain_calibration(
    value_df: data.ExperimentDF, calib_data: CalibrationData
) -> Tuple[data.ExperimentDF, Dict[str, float], Dict[str, float]]:
    # based on https://www.digikey.it/it/articles/using-an-accelerometer-for-inclination-sensing

    calibrated_df = value_df.copy(True)

    offsets = dict()
    gains = dict()

    for axis_name in ["x", "y", "z"]:
        pos_df, neg_df = getattr(calib_data, f"{axis_name}_pair")
        pos_df = pos_df.loc[pos_df.type == data.SensorType.ACCELEROMETER.value]
        neg_df = neg_df.loc[neg_df.type == data.SensorType.ACCELEROMETER.value]

        pos_axis_mean = pos_df[axis_name].mean()
        neg_axis_mean = neg_df[axis_name].mean()

        offsets[axis_name] = 0.5 * (pos_axis_mean + neg_axis_mean)
        gains[axis_name] = 0.5 * (pos_axis_mean - neg_axis_mean) / 9.81

        accel_rows = calibrated_df.type == data.SensorType.ACCELEROMETER.value
        calibrated_df.loc[accel_rows, axis_name] -= offsets[axis_name]
        calibrated_df.loc[accel_rows, axis_name] /= gains[axis_name]

    return calibrated_df, offsets, gains


def norm_df_with_min_and_max(
    min_and_max: Tuple[Tuple[float, float, float], Tuple[float, float, float]],
    means: List[float],
    df: pd.DataFrame,
) -> pd.DataFrame:

    # Real-time Distance Measuring Evaluation for Accelerometer Sensor on Smartphone
    # 10.1109/ECTI-CON47248.2019.8955374

    min, max = min_and_max
    normed_df = df.copy(True)

    normed_df.loc[normed_df.x > max[0], "x"] -= max[0] - means[0]
    normed_df.loc[(normed_df.x >= min[0]) & (normed_df.x <= max[0]), "x"] = 0
    normed_df.loc[normed_df.x < min[0], "x"] += means[0] - min[0]
    normed_df.loc[normed_df.y > max[1], "y"] -= max[1] - means[1]
    normed_df.loc[(normed_df.y >= min[1]) & (normed_df.y <= max[1]), "y"] = 0
    normed_df.loc[normed_df.y < min[1], "y"] += means[1] - min[1]
    normed_df.loc[normed_df.z > max[2], "z"] -= max[2] - means[2]
    normed_df.loc[(normed_df.z >= min[2]) & (normed_df.z <= max[2]), "z"] = 0
    normed_df.loc[normed_df.z < min[2], "z"] += means[2] - min[2]

    return normed_df
