import tempfile
from pathlib import Path
from typing import Optional

import numpy as np
import pandas as pd
import pandera as pa
from matplotlib import pyplot as plt
from scipy.interpolate import CubicSpline
from scipy.spatial.transform import Rotation
import open3d as o3d


class PointsAndNormalsSchema(pa.SchemaModel):
    point_x: pa.typing.Series[float] = pa.Field()
    point_y: pa.typing.Series[float] = pa.Field()
    point_z: pa.typing.Series[float] = pa.Field()
    normal_x: pa.typing.Series[float] = pa.Field()
    normal_y: pa.typing.Series[float] = pa.Field()
    normal_z: pa.typing.Series[float] = pa.Field()


PointsAndNormalsDF = pa.typing.DataFrame[PointsAndNormalsSchema]


class GyroInputSchema(pa.SchemaModel):
    timestamp: pa.typing.Series[int] = pa.Field()
    x: pa.typing.Series[float] = pa.Field()
    y: pa.typing.Series[float] = pa.Field()
    z: pa.typing.Series[float] = pa.Field()


GyroInputDF = pa.typing.DataFrame[GyroInputSchema]


class OrientationChangeSchema(pa.SchemaModel):
    timestamp: pa.typing.Series[int] = pa.Field()
    x: pa.typing.Series[float] = pa.Field()
    y: pa.typing.Series[float] = pa.Field()
    z: pa.typing.Series[float] = pa.Field()
    ori_diff_x: pa.typing.Series[float] = pa.Field()
    ori_diff_y: pa.typing.Series[float] = pa.Field()
    ori_diff_z: pa.typing.Series[float] = pa.Field()


OrientationChangeDF = pa.typing.DataFrame[OrientationChangeSchema]


@pa.check_types
def build_mesh_from_gyro_data(
    horizontal_circumference: float,
    vertical_circumference: float,
    left_values: GyroInputDF,
    right_values: GyroInputDF,
    up_values: GyroInputDF,
    down_values: GyroInputDF,
    model_extension: str = "glb",
) -> bytes:

    left_points_and_normals = _calc_points_and_normals(
        left_values, True, horizontal_circumference
    )
    right_points_and_normals = _calc_points_and_normals(
        right_values, True, horizontal_circumference
    )
    up_points_and_normals = _calc_points_and_normals(
        up_values, False, vertical_circumference
    )
    down_points_and_normals = _calc_points_and_normals(
        down_values, False, vertical_circumference
    )

    filler_df = _generate_connection_points(
        left_points_and_normals,
        right_points_and_normals,
        up_points_and_normals,
        down_points_and_normals,
    )

    combined_df = pd.concat(
        [
            left_points_and_normals,
            right_points_and_normals,
            up_points_and_normals,
            down_points_and_normals,
            filler_df,
        ]
    )

    pcd: o3d.geometry.PointCloud = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(
        combined_df[["point_x", "point_y", "point_z"]].to_numpy()
    )
    pcd.normals = o3d.utility.Vector3dVector(
        combined_df[["normal_x", "normal_y", "normal_z"]].to_numpy()
    )

    poisson_mesh, densities = o3d.geometry.TriangleMesh().create_from_point_cloud_poisson(pcd=pcd)

    densities = np.asarray(densities)
    density_colors = plt.get_cmap("plasma")(
        (densities - densities.min()) / (densities.max() - densities.min())
    )
    density_colors = density_colors[:, :3]
    density_mesh = o3d.geometry.TriangleMesh()
    density_mesh.vertices = poisson_mesh.vertices
    density_mesh.triangles = poisson_mesh.triangles
    density_mesh.triangle_normals = poisson_mesh.triangle_normals
    density_mesh.vertex_colors = o3d.utility.Vector3dVector(density_colors)

    with tempfile.TemporaryDirectory() as out_dir_name:
        out_file = Path(out_dir_name) / f"model.{model_extension}"
        o3d.io.write_triangle_mesh(out_file.as_posix(), density_mesh)
        return out_file.read_bytes()


@pa.check_types
def _generate_connection_points(
    left_value_df: PointsAndNormalsDF,
    right_value_df: PointsAndNormalsDF,
    up_value_df: PointsAndNormalsDF,
    down_value_df: PointsAndNormalsDF,
    num_steps: int = 100,
    num_rings: int = 100,
) -> PointsAndNormalsDF:

    def _lin_interpolate(
        start: np.ndarray, end: np.ndarray, num_interpolations: int
    ) -> np.ndarray:
        connection = end - start
        step = connection / num_interpolations
        points = list()
        for i in range(num_interpolations):
            points.append(start + i * step)
        return np.asarray(points)

    def _find_nearest(array: np.ndarray, value: float) -> int:
        return (np.abs(array - value)).argmin()

    def _find_pairs(*arrays: np.ndarray):
        end_z = max([a[-1][2] for a in arrays])

        step = end_z / num_rings
        z_range = np.arange(start=step, stop=end_z + step, step=step)

        indices = list()
        for z in z_range:
            indices.append([_find_nearest(a[..., 2], z) for a in arrays])
        indices = np.asarray(indices)

        return z_range, indices

    dir = [
        left_value_df[["point_x", "point_y", "point_z"]].to_numpy(),
        up_value_df[["point_x", "point_y", "point_z"]].to_numpy(),
        right_value_df[["point_x", "point_y", "point_z"]].to_numpy(),
        down_value_df[["point_x", "point_y", "point_z"]].to_numpy(),
    ]
    normals = [
        left_value_df[["normal_x", "normal_y", "normal_z"]].to_numpy(),
        up_value_df[["normal_x", "normal_y", "normal_z"]].to_numpy(),
        right_value_df[["normal_x", "normal_y", "normal_z"]].to_numpy(),
        down_value_df[["normal_x", "normal_y", "normal_z"]].to_numpy(),
    ]

    z_values, indices = _find_pairs(*dir)

    selected_points = list()
    selected_normals = list()
    for i in range(len(dir)):
        sel_points = dir[i][indices[:, i]]
        sel_points[:, 2] = z_values
        selected_points.append(sel_points)
        selected_normals.append(normals[i][indices[:, i]])

    constructed_points = list()
    constructed_normals = list()
    theta = 2 * np.pi * np.linspace(0, 1, 5)
    for i in range(num_rings):
        points_to_interp = list()
        for direction in selected_points:
            points_to_interp.append((direction[i][0], direction[i][1]))

        points_to_interp.append(
            points_to_interp[0]
        )  # add first point at the end again to close the circle
        points_to_interp = np.asarray(points_to_interp)

        cs = CubicSpline(theta, points_to_interp, bc_type="periodic")

        xs = 2 * np.pi * np.linspace(0, 1, num_steps)
        interp_values = cs(xs)
        new_x_values = interp_values[:, 0]
        new_y_values = interp_values[:, 1]

        z = np.full((num_steps), selected_points[0][i][2])
        constructed_points.append(
            np.stack(arrays=[new_x_values, new_y_values, z], axis=1),
        )

        normals_to_interp = list()
        for direction in selected_normals:
            normals_to_interp.append(direction[i])

        normals_to_interp.append(normals_to_interp[0])

        num_parts = len(normals_to_interp) - 1
        for j in range(num_parts):
            constructed_normals.append(
                _lin_interpolate(
                    start=normals_to_interp[j],
                    end=normals_to_interp[j + 1],
                    num_interpolations=num_steps // num_parts,
                )
            )

    result_df = pd.DataFrame(
        np.hstack(
            (np.concatenate(constructed_points), np.concatenate(constructed_normals))
        ),
        columns=["point_x", "point_y", "point_z", "normal_x", "normal_y", "normal_z"],
    )

    return result_df


@pa.check_types
def _calc_points_and_normals(
    value_df: GyroInputDF, horizontal: bool, arc_length: float
):

    ori_diff_df = _calc_orientation_diffs(value_df)

    pos_df = ori_diff_df.copy(True)
    pos_df["point_x"] = 0.0
    pos_df["point_y"] = 0.0
    pos_df["point_z"] = 0.0
    pos_df["normal_x"] = 0.0
    pos_df["normal_y"] = 0.0
    pos_df["normal_z"] = 0.0

    records = pos_df.to_records()

    total_time_diff = (records[-1].timestamp - records[0].timestamp) * 1e-9

    if horizontal:
        dir_factor = records[-1].ori_diff_y / abs(records[-1].ori_diff_y)
    else:
        dir_factor = records[-1].ori_diff_x / abs(records[-1].ori_diff_x)

    last_row = records[0]
    last_row.normal_z = 1.0

    for row in records[1:]:
        # if (abs(row.ori_diff_deg_y) <= abs(last_row.ori_diff_deg_y)):
        #     continue
        delta_time = (row.timestamp - last_row.timestamp) * 1e-9
        step_arc_length = (delta_time * arc_length) / total_time_diff

        if horizontal:
            angle = (row.ori_diff_y + last_row.ori_diff_y) / 2
            row.point_x = (
                np.cos(np.deg2rad(angle)) * step_arc_length * dir_factor
            ) + last_row.point_x
        else:
            angle = (row.ori_diff_x + last_row.ori_diff_x) / 2
            row.point_y = (
                np.cos(np.deg2rad(angle)) * step_arc_length * dir_factor * -1
            ) + last_row.point_y

        row.point_z = (
            np.sin(np.deg2rad(angle)) * step_arc_length * dir_factor * -1
        ) + last_row.point_z

        rotation = Rotation.from_euler(
            degrees=True,
            angles=(row.ori_diff_x, row.ori_diff_y, row.ori_diff_z),
            seq="xyz",
        )

        normal = rotation.apply((0.0, 0.0, 1.0))
        row.normal_x = normal[0]
        row.normal_y = normal[1]
        row.normal_z = normal[2]

        last_row = row

    pos_df = pd.DataFrame.from_records(records)
    pos_df = pos_df.drop(
        columns=[
            "index",
            "timestamp",
            "ori_diff_x",
            "ori_diff_y",
            "ori_diff_z",
            "x",
            "y",
            "z",
        ]
    )

    return pos_df


@pa.check_types
def _calc_orientation_diffs(value_df: GyroInputDF) -> Optional[OrientationChangeDF]:
    if len(value_df) < 2:
        return

    ori_diff_df = value_df.copy(True)
    ori_diff_df["ori_diff_x"] = 0.0
    ori_diff_df["ori_diff_y"] = 0.0
    ori_diff_df["ori_diff_z"] = 0.0
    ori_diff_df["ori_diff_rad_x"] = 0.0
    ori_diff_df["ori_diff_rad_y"] = 0.0
    ori_diff_df["ori_diff_rad_z"] = 0.0

    records = ori_diff_df.to_records()

    last_row = records[0]
    for row in records[1:]:
        delta_time = (row.timestamp - last_row.timestamp) * 1e-9

        # radian
        row.ori_diff_rad_x = delta_time * row.x + last_row.ori_diff_rad_x
        row.ori_diff_rad_y = delta_time * row.y + last_row.ori_diff_rad_y
        row.ori_diff_rad_z = delta_time * row.z + last_row.ori_diff_rad_z

        # degrees
        row.ori_diff_x = row.ori_diff_rad_x * 180 / np.pi
        row.ori_diff_y = row.ori_diff_rad_y * 180 / np.pi
        row.ori_diff_z = row.ori_diff_rad_z * 180 / np.pi

        last_row = row

    ori_diff_df = pd.DataFrame.from_records(records)
    ori_diff_df = ori_diff_df.drop(
        columns=["ori_diff_rad_x", "ori_diff_rad_y", "ori_diff_rad_z", "index"]
    )

    return ori_diff_df
