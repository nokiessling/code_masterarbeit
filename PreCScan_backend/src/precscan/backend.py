import io
import json
import time
from pathlib import Path

import pandas as pd
from flask import Flask, request, abort, send_file

from precscan.reconstruct import build_mesh_from_gyro_data

app = Flask(__name__)


@app.before_request
def only_json():
    if not request.is_json:
        abort(400)


@app.route("/reconstruct", methods=["POST"])
def reconstruct():
    req_obj = request.json

    # with Path(f"request_{time.time()}.json").open("w") as f:
    #     json.dump(req_obj, f)

    return send_file(
        path_or_file=io.BytesIO(
            build_mesh_from_gyro_data(
                req_obj["horizontalCircumference"],
                req_obj["verticalCircumference"],
                pd.DataFrame(req_obj["leftScanData"]),
                pd.DataFrame(req_obj["rightScanData"]),
                pd.DataFrame(req_obj["upScanData"]),
                pd.DataFrame(req_obj["downScanData"]),
            )
        ),
        mimetype="model/gltf-binary",
    )
