import argparse
import multiprocessing
import subprocess


def main():
    parser = argparse.ArgumentParser(
        prog="precscan-backend",
        description="HTTP backend server management script for the PreCScan APP.",
    )

    parser.add_argument(
        "--dev-server",
        action="store_true",
        help="If set starts the backend with the dev server with automatic code reloading",
    )
    parser.add_argument(
        "--address",
        type=str,
        default="127.0.0.1",
        help=(
            "Defines the address to which the backend server binds. By default 127.0.0.1 will be used which causes "
            "the server to be only reachable on this machine. If it should be reachable through all network "
            "interfaces use 0.0.0.0."
        ),
    )
    parser.add_argument(
        "--port",
        type=int,
        default=5000,
        help="Defines the port to which the backend server binds. By default 5000 will be used.",
    )
    parser.add_argument(
        "--num-workers",
        type=int,
        default=multiprocessing.cpu_count(),
        help=(
            "Defines the number of worker processes if the release server is used. By default num-workers is equal "
            "to the number of cpu cores"
        ),
    )

    args = parser.parse_args()

    if args.dev_server:
        cmd = (
            "flask --app precscan.backend --debug run "
            f"--host {args.address} "
            f"--port {args.port}"
        )
    else:
        cmd = (
            "gunicorn "
            f"--workers {args.num_workers} "
            f"--bind {args.address}:{args.port} "
            "precscan.backend:app"
        )

    subprocess.run(cmd.split())
