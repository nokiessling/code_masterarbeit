"""
This module just exists for compatibility reasons. all configuration is done in `pyproject.toml`

https://setuptools.pypa.io/en/latest/userguide/pyproject_config.html
"""

from setuptools import setup

setup()
