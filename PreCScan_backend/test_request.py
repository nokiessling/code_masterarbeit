#!/usr/bin/env python3

import json
import tempfile
from pathlib import Path

import requests
import open3d as o3d


script_dir = Path(__file__).parent

response = requests.post(
        url="http://127.0.0.1:5000/reconstruct",
        json=json.loads((script_dir / "p6_sample_1.json").read_text("utf-8")))

if response.status_code != 200:
    raise RuntimeError(f"Received a {response.status_code} as status")

with tempfile.TemporaryDirectory() as out_dir_name:
    # currently open3d can't load glb files. therefore you will have to configure the backend to generate .obj files if
    # you want to run this test.
    out_file = Path(out_dir_name) / "model.obj"
    out_file.write_bytes(response.content)
    mesh = o3d.io.read_triangle_mesh(out_file.as_posix())

    o3d.visualization.draw_geometries([mesh])

