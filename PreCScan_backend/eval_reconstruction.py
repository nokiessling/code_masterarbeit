#!/usr/bin/env python3
import json
import math
from pathlib import Path
from typing import Tuple

import pandas as pd

import precscan.reconstruct as reconstruct


def calculate_diameter_and_height(
    raw_value_one_df: reconstruct.GyroInputDF,
    raw_value_two_df: reconstruct.GyroInputDF,
    horizontal: bool,
    circumference: float,
) -> Tuple[float, float]:

    value_one_df = reconstruct._calc_points_and_normals(
        raw_value_one_df, horizontal, circumference
    )
    value_two_df = reconstruct._calc_points_and_normals(
        raw_value_two_df, horizontal, circumference
    )
    value_one_last = value_one_df.iloc[-1]
    value_two_last = value_two_df.iloc[-1]

    if horizontal:
        diameter = 0.5 * (abs(value_one_last.point_x) + abs(value_two_last.point_x))
    else:
        diameter = 0.5 * (abs(value_one_last.point_y) + abs(value_two_last.point_y))
    height = abs(min(value_one_last.point_z, value_two_last.point_z))
    return diameter, height


def evaluate_json_request(name: str):
    with Path(__file__).with_name(name).open("r") as f:
        req_json = json.load(f)

    horizontal_diameter, horizontal_height = calculate_diameter_and_height(
        raw_value_one_df=pd.DataFrame(req_json["leftScanData"]),
        raw_value_two_df=pd.DataFrame(req_json["rightScanData"]),
        horizontal=True,
        circumference=req_json["horizontalCircumference"],
    )

    vertical_diameter, vertical_height = calculate_diameter_and_height(
        raw_value_one_df=pd.DataFrame(req_json["upScanData"]),
        raw_value_two_df=pd.DataFrame(req_json["downScanData"]),
        horizontal=False,
        circumference=req_json["verticalCircumference"],
    )

    height = max(horizontal_height, vertical_height)
    print(f"{name}: {horizontal_diameter=}, {vertical_diameter=}, {height=}")


def main():
    target_diameter = 300
    target_height = 155
    print(f"{target_diameter=}, {target_height=}")
    evaluate_json_request("p6_sample_1.json")
    evaluate_json_request("p6_sample_2.json")
    evaluate_json_request("p6_sample_3.json")
    evaluate_json_request("p6_sample_4.json")
    evaluate_json_request("op7p_sample_1.json")
    evaluate_json_request("op7p_sample_2.json")
    evaluate_json_request("op7p_sample_3.json")
    evaluate_json_request("op7p_sample_4.json")


if __name__ == "__main__":
    main()
